import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor() {}
  affichage;
  resultat;

  ngOnInit() {
    this.affichage = document.getElementById("blocAffichage");
    this.resultat = document.getElementById("blocResultat");
  }
  number = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  operator = [" + ", " - ", " * ", " / ", "/ 100", "."];
  
  moins = "-";
  //Affiche les nombres

  click0() {
    this.affichage.innerHTML += this.number[0];
  }

  click1() {
    this.affichage.innerHTML += this.number[1];
  }

  click2() {
    this.affichage.innerHTML += this.number[2];
  }

  click3() {
    this.affichage.innerHTML += this.number[3];
  }

  click4() {
    this.affichage.innerHTML += this.number[4];
  }

  click5() {
    this.affichage.innerHTML += this.number[5];
  }

  click6() {
    this.affichage.innerHTML += this.number[6];
  }

  click7() {
    this.affichage.innerHTML += this.number[7];
  }

  click8() {
    this.affichage.innerHTML += this.number[8];
  }

  click9() {
    this.affichage.innerHTML += this.number[9];
  }

  //Affiche les operators

  clickPlus() {
    this.affichage.innerHTML += this.operator[0];
  }

  clickMoins() {
    this.affichage.innerHTML += this.operator[1];
  }

  clickMultiplier() {
    this.affichage.innerHTML += this.operator[2];
  }

  clickDiviser() {
    this.affichage.innerHTML += this.operator[3];
  }

  clickPourcent() {
    this.affichage.innerHTML += this.operator[4];
  }

  clickPoint() {
    this.affichage.innerHTML += this.operator[5];
  }

  clickC() {
    this.affichage.innerHTML = "";
    this.resultat.innerHTML = "";
  }

  clickMoinsPlus() {
    this.affichage.innerHTML = eval(this.affichage.textContent + "*-1");
  }

  // Affiche le résultat
  clickResultat() {
    this.resultat.innerHTML = eval(this.affichage.textContent);
  }

}
